import "./App.scss";
import Header from "./pages/Header";
import { Routes, Route } from "react-router-dom";
import MainCv from "./pages/MainCv";

function App() {
  return (
    <div className="app">
      <Routes>
        <Route path="/" element={<Header />} />
        <Route path="/cv" element={<MainCv />} />
      </Routes>
    </div>
  );
}

export default App;
