import React from "react";
import "./About.scss";
import { useSelector } from "react-redux";

const About = () => {
  
  const { aboutMe } = useSelector((state) => state.cv.hero);

  return (
    <div className="about">
      <h3>About</h3>
      <p>{aboutMe}</p>
    </div>
  );
};

export default About;
