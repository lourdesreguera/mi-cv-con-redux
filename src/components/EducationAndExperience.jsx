import React from "react";
import EducationAndExperienceEl from "./EducationAndExperienceEl";
import "./EducationAndExperience.scss";
import { useSelector } from "react-redux";

const EducationAndExperience = () => {
  const { sectionInfo } = useSelector(state => state.cv)
  return (
    <div className="educationAndExperience">
      {sectionInfo.map((info, i) => {
        return <EducationAndExperienceEl data={info} key={i} />;
      })}
    </div>
  );
};

export default EducationAndExperience;
