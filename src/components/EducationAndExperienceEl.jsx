import React from "react";
import "./EducationAndExperience.scss";

const EducationAndExperienceEl = ({ data }) => {
  return (
    <div className="educationAndExperience__container">
      <div className="educationAndExperience__container--date">
        <p>{data.date}</p>
      </div>
      <div className="educationAndExperience__container--text">
        <p className="educationAndExperience__container--name">
          {data.name}
        </p>
        <p>{data.where}</p>
        {data.description && (
          <p className="educationAndExperience__container--desc">
            {data.description}
          </p>
        )}{" "}
      </div>
    </div>
  );
};

export default EducationAndExperienceEl;
