import React from "react";
import "./Navbar.scss";
import { useDispatch } from "react-redux/es/hooks/useDispatch";
import { setSectionData } from "../redux/cv/cv.actions";

const Navbar = () => {
  const dispatch = useDispatch();

  return (
    <div className="navbar">
      <nav>
        <ul>
          <li onClick={() => dispatch(setSectionData('education'))}>Formación</li>
          <li onClick={() => dispatch(setSectionData('experience'))}>Experiencia</li>
        </ul>
      </nav>
    </div>
  );
};

export default Navbar;
