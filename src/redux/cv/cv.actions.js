export const SET_SECTION_DATA = 'SET_SECTION_DATA';

export const setSectionData = (sectionName) => dispatch => {
    dispatch({
        type: SET_SECTION_DATA,
        payload: sectionName
    })
}
