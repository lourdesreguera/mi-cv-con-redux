import * as actions from "./cv.actions";
import CV from "../../datos/CV";

const { hero, education, experience, languages, skills } = CV;

const INITIAL_STATE = {
  hero,
  education,
  experience,
  languages,
  skills,
  sectionInfo: education,
  sectionName: "education",
};

const cvReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actions.SET_SECTION_DATA: {
      return {
        ...state,
        sectionInfo: state[action.payload],
        sectionName: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};

export default cvReducer;
